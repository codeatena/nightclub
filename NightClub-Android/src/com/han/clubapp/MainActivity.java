package com.han.clubapp;

import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class MainActivity extends TabActivity {

	public static ArrayList<ClubInfo> arrClub = new ArrayList<ClubInfo>();
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        initValue();
        initWidget();
    }
	
	private void initWidget() {
        TabHost tabHost = getTabHost();
        
        TabSpec specHome = tabHost.newTabSpec("Home");
        View view1 = createTabView(tabHost.getContext(), "Home");
        specHome.setIndicator(view1);
   
        Intent homeIntent = new Intent(this, HomeActivity.class);
        specHome.setContent(homeIntent);
        
        TabSpec specClubList = tabHost.newTabSpec("Clubs");
//        specClubList.setIndicator("Clubs");
        View view2 = createTabView(tabHost.getContext(), "Clubs");
        specClubList.setIndicator(view2);
        Intent clubsIntent = new Intent(this, ClubListActivity.class);
        specClubList.setContent(clubsIntent);
        
        TabSpec specTop = tabHost.newTabSpec("Top Ten");
        View view3 = createTabView(tabHost.getContext(), "Top Ten");
        specTop.setIndicator(view3);
        Intent topInetent = new Intent(this, ClubTopActivity.class);
        specTop.setContent(topInetent);
        
        TabSpec specNearBy = tabHost.newTabSpec("NearBy");
        View view4 = createTabView(tabHost.getContext(), "NearBy");
        specNearBy.setIndicator(view4);
        Intent nearbyInetent = new Intent(this, NearByActivity.class);
        specNearBy.setContent(nearbyInetent);
        
        // Adding all TabSpec to TabHost
        tabHost.addTab(specHome);
        tabHost.addTab(specClubList);
        tabHost.addTab(specTop);
        tabHost.addTab(specNearBy);
        
//        tabHost.getTabWidget().getChildTabViewAt(0).setBackgroundResource(R.drawable.tab_bg_selector);
//        tabHost.getTabWidget().getChildTabViewAt(0).setBackgroundResource(R.drawable.tab_bg_selector);
//        tabHost.getTabWidget().getChildTabViewAt(1).setBackgroundResource(R.drawable.tab_bg_selector);
//        tabHost.getTabWidget().getChildTabViewAt(2).setBackgroundResource(R.drawable.tab_bg_selector);
//        tabHost.getTabWidget().getChildTabViewAt(3).setBackgroundResource(R.drawable.tab_bg_selector);
         
	}
	
	private static View createTabView(final Context context, final String text) {
	    View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
	    TextView tv = (TextView) view.findViewById(R.id.tabsText);
	    ImageView ti = (ImageView) view.findViewById(R.id.tab_icon_image);
	    
	    tv.setText(text);
	    
	    if (text.equals("Home")) {
	    	ti.setImageResource(R.drawable.tab_home);
	    }
	    if (text.equals("Clubs")) {
	    	ti.setImageResource(R.drawable.tab_club);
	    }
	    if (text.equals("Top Ten")) {
	    	ti.setImageResource(R.drawable.tab_top);
	    }
	    if (text.equals("NearBy")) {
	    	ti.setImageResource(R.drawable.tab_nearby);
	    }
	    
	    return view;
	}
	
	private void initValue() {
		ClubInfo club = null;

		arrClub = new ArrayList<ClubInfo>();
		club = new ClubInfo("360", "We have been voted in the Top 100 clubs in the world for three years running by UK DJ Magazine. " +
				"Earlier this year 360� also scooped up the Time Out Dubai Nightlife Award for best club in Dubai and won �Best Club� for nightlife hot spot at the Ahlan�s! Best in Dubai Awards 2013." +
				"Winter is officially here with the opening of the Rooftop lounge and the start of the dinner" +
				" and brunch concept �Season VIII� features some of the best DJ�s from Resident DJ Tristan Bain to internationally recognized guest" +
				" DJ�s such as DJ Hybrid. We are certain this will be a season to remember!" +
				"For entry before 20:00 on Fridays and Saturdays, register at Platinum List. " +
				"Please note that Jumeirah Beach Hotel guests will always be allowed entry." +
				"All visitors must be 21 or above. You must have an original ID, Passport, or Driver license.", R.drawable.club1, new LatLng(25.145732, 55.188611));
		arrClub.add(club);
		
		club = new ClubInfo("Armani Prive", "Launched by fashion icon Giorgio Armani, Armani Priv� is one of the chicest new venues to hit Dubai. " +
				"Exuding in exclusivity, elegance and excellence, this high end VIP lounge envisages a new, " +
				"luxurious clubbing experience concept. " +
				"Its slick, smooth black and white d�cor encompasses a lounge area, " +
				"bar and dance floor where one can dance away into the early hours. " +
				"For anyone is looking for complete clubbing experience en vogue, " +
				"Armani Priv� Dubai is the place to be. " +
				"\n\n\n\nAddress: Armani Hotel Dubai Downtown Burj Khalifa Dubai", 
				R.drawable.club11, new LatLng(25.197216, 55.273955));
		arrClub.add(club);
		
		//Bitmap bmp2 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club2);
		club = new ClubInfo("AT.Mosphere", "Towering at over 442 metres high on the 122nd floor of the world�s tallest building, " +
				"At.mosphere Lounge offers a memorable and unique dining experience." +
				"Located below the top floor observatory lounge Burj Khalifa, " +
				"this innovative venue holds the Guinness world record for highest restaurant from ground level and boasts a spectacular view of Dubai " +
				"and the Arabian Gulf.  Guests have the choice between unwinding with drinks in the chilled-out lounge and savouring " +
				"Michelin star cuisine in the grill restaurant. Whether you�re a fine dining connoisseur, lounge lover or just there for the view," +
				"At.mosphere Lounge is the must-visit restaurant for everyone." +
				"\n\n\n\nAdress: Burj Khalifa Downtown Dubai", R.drawable.club2, new LatLng(25.197311, 55.273708));
		arrClub.add(club);
		
		//Bitmap bmp3 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club3);
		club = new ClubInfo("Blue Marlin", "Imported from the smooth shores of the Mediterranean," +
				"Blue Marlin Dubai Beach Club encompasses the true Ibiza lifestyle." +
				"This exclusive beach club is found right in the heart of Dubai," +
				"offering the ultimate party experience from Spain�s most infamous clubbing island." +
				"Guests embrace the Ibiza spirit with a belated breakfast or lazy lunch by the poolside before taking a dip in the cool, deep blue sea." +
				"Treated to a fusion of Mediterranean and Japanese cuisine and a selection of first class cocktails," +
				"guests at Blue Marlin Ibiza are pampered to the max.  Upon sunset the relaxed beach vibe suddenly transforms into buzzing club," +
				"with nightfall bringing a change of playlist, switching from chilled tunes to banging house.  Offering fine cuisine day and night," +
				"smoothly shaken cocktails, poolside lounging and beach party antics, Blue Marlin Dubai Beach Club definitely provides the whole relaxation package." +
				"\n\n\n\nAddress: Right off Sheikh Zayed road , Ghantoot Exit 399 in Golden Tulip Al Jazira Hotels and Resort, 25 minutes from Mall of the Emirates towards Abu Dhabi.", R.drawable.club3
				, new LatLng(24.85782, 54.89719));
		arrClub.add(club);
		
		//Bitmap bmp4 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club4);
		club = new ClubInfo("Boutiq", "Boutiq Ultra Lounge is the brainchild of some of the global nightlife industry�s movers and shakers." +
				"Pooling their international experience from their venues in Miami, Washington, Sydney, London and Kiev," +
				"they�ve selected the best in its class elements to create Downtown's ultimate nightlife destination." +
				"The venue, with a total area of 5,000 square feet, has three main sections.The Gold Room located on the first floor," +
				"offers an intimate setting and perfect ambiance to chill out, with signature cocktails, an extensive bar and light grazing menu," +
				"good for weekday wind downs, or starting off the weekend." +
				"The Main Room is hosts a line up of international and local DJ's, with THERAPY Mondays, and ROCK YOUR SANITY Fridays quickly becoming nightlife hotspots." +
				"The VVIP balcony offers an overview of the main room, while being raised above the crowds, to offer the experience of exclusivity amidst the party," +
				"on level with the raised DJ Booth, and overlooking the dance floor enhancing the nightlife experience." +
				"\n\n\n\nAdress: The address hotel Dubai Mall Dubai", R.drawable.club4, new LatLng(25.199672, 55.277525));
		arrClub.add(club);
		
		//Bitmap bmp5 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club5);
		club = new ClubInfo("Buddha Bar", "Originating from one of Paris�s chicest quartiers, " +
				"Buddha Bar has now been rated as one of Dubai�s best restaurants and bars." +
				"This majestic venue located in the heart of the Marina houses an impressive two-floor restaurant" +
				"as well as a bar serving fancy cocktails and a fine selection of bubbly.    Guests dine in the extravagant dining room," +
				"lined with plush carpets, adorned with rich red and luxurious gold d�cor and dazzling from the light sparkling from overhanging crystal chandeliers." +
				"Oozing in sophistication and style, Buddha Bar�s eye-catching and unique design makes it a quite unforgettable venue to visit." +
				"\n\n\n\nAdress:Grosvenor House Dubai, Dubai Marina, Dubai", R.drawable.club5, new LatLng(25.085909, 55.144178));
		arrClub.add(club);
		
		//Bitmap bmp6 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club6);
		club = new ClubInfo("Cavalli Club", "Launched by high-end designer Roberto Cavalli in Dubai," +
				"Cavalli Club is the true definition of glitz and glam." +
				"Situated in the Fairmont Hotel and spanning over three floors," +
				"this opulent venue attracts a select clientele who know how to party in style." +
				"Serving the finest gastronomy, award-winning cocktails and showcasing a collection of Cavalli�s couture," +
				"Cavalli Club Dubai offers the classiest of nights out.  Its exclusivity draws in many a famous face," +
				"with previous events welcoming Bob Sinclair, Carmen Electra, Chris Brown and AKON.", R.drawable.club6, new LatLng(25.057038, 55.125857));
		arrClub.add(club);
		
		club = new ClubInfo("Embassy", "At the top of Tower Two, placed over three entire floors with unrivalled 360� panoramas is the sleek and sassy Embassy Dubai. " +
				"A heady mix of opulence, the city's first international and exclusive late night venue will combine dining, drinking, " +
				" dancing and offer exquisite modern European cuisine, exemplary service and exceptional DJ's. ", 
				R.drawable.club12, new LatLng(25.085462, 55.143674));
		arrClub.add(club);
		
		club = new ClubInfo("Hard Rock Cafe", "Hard Rock Cafe is now open in Dubai! Located just off the Business Bay Crossing at Festival Centre, Dubai Festival City, " +
				"which is one of the city�s premier shopping malls. " +
				"A few minutes away from Dubai International Airport, " +
				"Dubai Festival City sits on the edge of Dubai Creek on the Deira side of the city. " +
				"The new Hard Rock Caf� Dubai includes a vibrant bar, " +
				"convertible live music space and a 2500ft Rock Shop featuring Hard Rock�s limited-edition merchandise. " +
				"Diners can take a first look at new memorabilia items from artists including Shakira, Aerosmith, " +
				"Prince and The Who. " +
				"It also offers two Rock Shops, a stage for live performances, a private VIP function room, " +
				"a separate lounge adjacent to the bar, and an outdoor terrace. " +
				"The Cafe can welcome 350 persons!", 
				R.drawable.club13, new LatLng(25.223981, 55.352112));
		arrClub.add(club);
		
		club = new ClubInfo("Infiniti Bar & Terrace", "Infiniti Bar & Terrace is a cosy spot to relax with a refreshing " +
				"beverage while enjoying beautiful views of the Burj Khalifa and Business Bay water canal. " +
				"The bar features a warm atmosphere with welcoming dark wood walls and floor, " +
				"a central bar, TV screens and a large outdoor terrace. " +
				"\n\n\n\nAdress: Radisson Blu Hotel, Dubai Downtown" +
				"\n\n\nOpening hours:" +
				"\n\nNoon - 01:00 (Sat - Wed)" +
				"\nNoon - 02:00 (Thu - Fri)" +
				"\n\n\nContact:" +
				"\n\nTel: +971 4 450 2000" +
				"\nFax: +971 4 450 2099", 
				R.drawable.club14, new LatLng(25.194151, 55.28955));
		arrClub.add(club);
		
		//Bitmap bmp7 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club7);
		club = new ClubInfo("Mahiki", "For any cocktail lovers, Mahiki is the ultimate destination.  Since its creation in 2005," +
				"this London export has become a world -famous bar whose concept has travelled the globe." +
				"Surrounded by tropical themed d�cor, fruity and fresh cocktails and grill food menu, " +
				"guests feel as though they have  been whisked away to a deserted exotic island. " +
				"Mahiki is particularly renowned for its famous cocktails, shaken and served by expert mixologists and" +
				"served in their signature tiki cups.  Mahiki�s infamous treasure chests are also popular with guests." +
				"Deemed as one of London�s most popular clubs and known for attracting an exclusive clientele, Mahiki attracts many a famous face.",
				R.drawable.club7, new LatLng(25.141671, 55.190934));
		arrClub.add(club);
		
		//Bitmap bmp8 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club8);
		club = new ClubInfo("Movida", "With an illustrious history that includes a string of awards and a dedicated following across Europe," +
				" Mo*Vida is the club that Dubai has been longing for." +
				" A clubbing pioneer, Mo*Vida set a new level in club experience when it opened in London in 2005," +
				"and was the first to bring a sophisticated blend of St Tropez service and European styling to the marketplace." +
				"Mo*Vida in Dubai is located in one of the city�s best sites, set against the dramatic back drop of the Sheikh Zayed Road," +
				" within the recently opened and highly acclaimed Radisson Royal Hotel. Located on the 1st floor, " +
				"the club boasts two bar areas and 38 VIP tables." +
				"Most tables look out onto the club floor, " +
				"playing perfectly into Dubai�s mantra of �See and be Seen�.", R.drawable.club8, new LatLng(25.223319, 55.282346));
		arrClub.add(club);
		
		club = new ClubInfo("Nasimi Beach Dubai", "If you are in need of a night of peace, calm and serenity, " +
				"Nasimi Beach is the perfect venue. " +
				"Located at the foot of iconic Atlantis hotel on Palm Island, " +
				"this classy venue draws in Dubai�s socialite as well as world jetsetters. " +
				"Nasimi Beach boasts a spectacular view of the Arabian skyline, " +
				"where guests take refuge and enjoy the ultimate chill-out experience. " +
				"Regulars lounge and laze around on plush white sofas and day beds on the shoreline terrace, " +
				"whose twin bars keep everyone�s thirst quenched with fresh, " +
				"fruity cocktails. " +
				"After an aperitif, guests savour the sumptuous selection of Mediterranean dishes ranging from seafood, " +
				"salad and grilled items as well as sushi.  " +
				"Nasimi Beach is the perfect place to chill during the day, " +
				"which comes to life after sunset with resident DJs pumping out the latest beats. " +
				"\n\n\n\nAddress: Atlantis Hotel, Palm Jumeirah Dubai", 
				R.drawable.club15, new LatLng(25.130509, 55.1172));
		arrClub.add(club);
		
		club = new ClubInfo("N'dulge", "Taking Dubai into the forefront of nightlife, N�Dulge, formerly known as Sanctuary, " +
				"has brought an exclusively new socialising concept to the region.  " +
				"Located on the exquisitely chic Palm Island, " +
				"this state-of-the-art club consists of three areas each offering a different vibe: N�Dulge Arena, " +
				"Lounge and Terrace.  " +
				"Open on Thursdays and Fridays, the N�Dulge Arena offers a spectacle like no other, featuring performers, " +
				"burlesque showgirls and dancers. " +
				"In contrast to the lively goings-on in the Arena, N�Dulge�s lounge offers a more chilled-out ambiance, " +
				"playing deep grooves, chilled house and outskool vibes.  " +
				"It is also home to the world-renowned restaurant Nobu, serving mouth-water-watering sushi and sashimi. " +
				"Now recognised as the pioneer for al fresco clubbing in Dubai, " +
				"the N�Dulge Terrace offers a divine view of the region�s lush landscapes whilst guests savour sushi, " +
				"smoke Shisha and sip on sumptuous beverages under the Arabian night sky. " +
				"\n\n\n\nAddress: Atlantis Hotel, Palm Jumeirah Dubai", 
				R.drawable.club16, new LatLng(25.130509, 55.1172));
		arrClub.add(club);
		
		//Bitmap bmp9 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club9);
		club = new ClubInfo("Okku", "Bringing a new style of contemporary Japanese cuisine to the Middle East with sophistication and style," +
				"Okku Dubai is a high class restaurant located in the iconic One Sheikh Zayed Road at the luxurious Monarch Dubai Hotel." +
				"Okku�s sleek and slick style prides itself on its high standard of service, with professional staff dedicated to giving guests the full five-star treatment." +
				"Drawing inspiration from the Japanese culinary A-list based in New York, Los Angeles and Las Vegas," +
				"Okku envisages a unique menu of divine delicacies.", 
				R.drawable.club9, new LatLng(25.230226, 55.286879));
		arrClub.add(club);
		
		club = new ClubInfo("People By Crystal", "Created by The Crystal Group, an enterprise synonymous with first-rate hospitality, " +
				"sophistication and style, People by Crystal Nightclub is one of the hottest social spots in Dubai. " +
				"Since its creation in 2003, The Crystal Group has become a cutting-edge pioneer in hospitality, " +
				"with its innovative concepts in clubbing, bar and restaurants spanning across Lebanon, Dubai and Europe. " +
				"This award-winning venue offers an unparalleled night out experience that you will never forget. " +
				"People club captivates guests immediately upon entry with its breath taking interior, " +
				"located inside a glass pyramid boasting an array of lighting effects which show off its impressive architecture comprising of bars, " +
				"columns and most importantly its spectacular 360 degree panoramic view of Dubai. " +
				"\n\n\n\nAddress: 18th Floor Raffles Hotel, Wafi City Dubai", 
				R.drawable.club17, new LatLng(25.227848, 55.319638));
		arrClub.add(club);
		
		club = new ClubInfo("Purobeach Dubai", "Pureobeach is located on the 6th floor of the chic new Conrad Dubai luxury hotel. " +
				"It's the ultimate escape within the city as you can enjoy a pure world of white lounges and blue waters. " +
				"Discreet, modern, elegant and in a laid back atmosphere, Purobeach is a perfect location for spending all day " +
				"and enjoying an evening with your best relatives. " +
				"Purobeach Dubai is a unique place with more than 3000 square meters of lounge terrace offering food " +
				"and drinks as well as privileged sun bed services. " +
				"Purobeach proposes a complete food offer fresh salads, " +
				"grilled fish and best cuts of meat not to mention its signature dishes. " +
				"This chic and casual poolside hangout offers an idyllic spot to sit back and tan or to enjoy a magical Arabian sunset with a cocktail. " +
				"Daily DJ sessions will ease guests to chill out from early afternoon until the night... " +
				"Take a deep breath, look at the water and enjoy your time at Purobeach. ", 
				R.drawable.club18, new LatLng(25.225563, 55.284034));
		arrClub.add(club);
		
		club = new ClubInfo("Rock Bottom", "Although it�s officially a bar and restaurant rather than a club, " +
				"Rock Bottom only really comes alive as other bars kick out. RBC pulls in an impressive crowd " +
				"with its proven blend of mixed drinks and a resident DJ and live band, " +
				"who pump out the crowd-pleasers until closing time. " +
				"There�s even an in-house shawarma joint for dancers with the munchies.", 
				R.drawable.club19, new LatLng(25.264499, 55.31162));
		arrClub.add(club);
		
		club = new ClubInfo("Shades Dubai", "The Shades is located on the 4th floor, overlooking the pool, " +
				"the stunning views of Dubai." +
				"You can enjoy tapas and � la carte offerings, inspired by cuisines from the Mediterranean to the Far East. " +
				"The resident DJ, Ren�e Hamilton and Saxophonist, Julia Wray,  step up the beat the weekend" +
				"\n\nOpening Hours:" +
				"\n\nWeekdays : 6 pm to 1 am" +
				"\nWeekends:  6pm to 2am" +
				"\n\n\n\nAddress: The Address, Dubai Marina, Dubai Marina, Dubai", 
				R.drawable.club20, new LatLng(25.077282, 55.140411));
		arrClub.add(club);
		
		club = new ClubInfo("Sho Cho", "Trendy Japanese restaurant and Lounge with a minimalist chic underwater atmosphere " +
				"that fuses traditional Japanese cuisine with western style dishes. " +
				"The cool interior is designed to reflect the aquatic feel of the menu � " +
				"cream leather furniture is matching with blue lighting and stunning, " +
				"almost architectural floral arrangements with ingenious fish tanks, " +
				"that all give you the overall effect of being in an unbelievably stylish and specious submarine." +
				"\n\n\n\nAddress: Dubai Marine Beach Resort & Spa, Jumeirah, Dubai", 
				R.drawable.club21, new LatLng(25.23447, 55.26481));
		arrClub.add(club);
		
		club = new ClubInfo("SkyView", "This is a wonderful location for afternoon tea, and pre- and post-dinner drinks that are served within an ambience of luxury and comfort. " +
				"\n\n\nEntertainment :" +
				"\n\nAfif Jazz Band from Tuesday to Sunday (8pm to 12am)" +
				"\nOlena Hutbulyak (Pianist) from Saturday to Thursday (1pm to 4pm)" +
				"\n\n\nCapacity : Lounge - 70 persons / Bar - 12 persons" +
				"\nDress code : Smart Casual" +
				"Experiences : Cocktails, Food served, Great view, VIP areas" +
				"Times : Open daily noon-2am" +
				"Minimum age for the Skyview Bar is 21 years." +
				"\n\n\n\nAddress: Burj Al Arab Dubai", 
				R.drawable.club22, new LatLng(25.141302, 55.185463));
		arrClub.add(club);
		
		club = new ClubInfo("Spin", "Feeling a little guilty lately about your amount of partying and lack of exercise?  " +
				"Why not combine the two at SPiN, the latest social networking concept to hit Dubai.  " +
				"Branded as a �galaxy of Ping-Pong social clubs�, " +
				"SPiN is part of an American franchise which set the trend for large sport appeal in lively social settings. " +
				"This stylish new venue boasts eleven state-of-art designer Ping-Pong tables as well as a luxury bar and delicious cuisine. " +
				"Aiming to unite sports and socialising, this unique concept is beginning to extend over the North American border. " +
				"Hang out with friends and have a go at the Ping-Pong tables whilst sipping on cocktails and savouring a snack. " +
				"The food menu offers a range of classy snacks � finger food is needed when playing Ping-Pong after all!" +
				"\n\n\n\nAddress: WAFI Pyramids, Dubai", 
				R.drawable.club23, new LatLng(25.229248, 55.320034));
		arrClub.add(club);
		
		club = new ClubInfo("Studio F", "Known for its style and sophistication, Dubai is the ideal place to welcome Studio F, " +
				"the latest and hottest venue to hit the city.  " +
				"Launched by Fashion TV, this new and exclusive fashion studio concept is the true definition of glitz and glam.  " +
				"With its prime location on the upper floor of the Boulevard Jumeirah Emirates Towers, " +
				"Studio F attracts both fashion icons and celebs alike who wine, " +
				"dine and lounge whilst soaking up everything that�s � la mode. " +
				"Want to feel like a VIP and receive five-star treatment?  " +
				"Then take a dive into the fabulous world of fashion and feel like a model strutting along the runway at Studio F. " +
				"\n\n\n\nAddress:  Emirates Towers Dubai", 
				R.drawable.club24, new LatLng(25.21754, 55.282323));
		arrClub.add(club);
		
		//Bitmap bmp10 = BitmapFactory.decodeResource(this.getResources(), R.drawable.club10);
		club = new ClubInfo("The Act", "Known for its risqu� stage performances, Las Vegas export The Act has raised its curtains in Dubai. " +
				" As the region�s first theatre club, The Act is one of the most sought out hotspots in town." +
				"Located on the 42nd and 43rd floors of the city's Shangri-La Hotel on Sheikh Zayed Road, " +
				" it is also the highest theatre is the world.  This unique theatrical concept brought to Dubai" +
				" by Simon Hammerstein offers an impressive range of variety acts from professional international performers." +
				"The Act offers the complete theatrical night out package with banquet booths lining the centre," +
				" dining tables in front of the main stage, enclaves and especially themed areas for private parties." +
				" Every night is filled with breath taking acts building up to the main show whilst guests savour Peruvian food and sip on cocktails."
				, R.drawable.club10, new LatLng(25.208407, 55.271861));
		arrClub.add(club);
		
		club = new ClubInfo("Trilogy", "3 levels of pure entertainment, the Ground Floor, the First Floor & The Rooftop, " +
				"make this one of Dubai�s trendy place. " +
				"With a line-up of top DJ�s spinning cutting-edge music, " +
				"Trilogy hosts some of the finest electronic music acts in the whole world. " +
				"Trilogy is situated at the entrance of Souk Madinat Jumeirah !" +
				"\n\n\nDress Code : chic in accordance with the UAE guidelines" +
				"\nTimes :  Daily Open 10.00pm - 3.00am (closed on Sundays)" +
				"\nMonday to Saturday, 10.00 pm to 3.00 am" +
				"\nThe Rooftop is open from Monday to Saturday" +
				"\nThe Club is open on Tuesday, Thursday and Friday" +
				"\nAccess policy: 21 years or above" +
				"\n\n\n\nAddress: Souk Madinat Jumeirah,Dubai", 
				R.drawable.club25, new LatLng(25.133344, 55.186697));
		arrClub.add(club);
		
		club = new ClubInfo("Vanity", "Vanity is a sensualist�s playground of controlled chaos, " +
				"precisely like the jewel box fantasies that fuel it. " +
				"Located in heartbeat of Dubai night life at the 5 star hotel al Murooj Rotana opposite to Dubai Mall and few minute " +
				"from DIFC with one of the best location for people �to see and be seen� " +
				"Al Murooj Rotana raises the bar for sophistication, luxury and indulgence in Dubai. " +
				"With a glamorous look and tropical ambience, " +
				"the luxury Dubai hotel, stands out as an oasis of chic, " +
				"providing passers-by with just a hint of a provocative and seductive interior scene within." +
				"\n\n\n\nAddress: Al Murooj Rotana, Sheikh Zayed Road, Dubai", 
				R.drawable.club26, new LatLng(25.202689, 55.277764));
		arrClub.add(club);
		
		club = new ClubInfo("Vip Room", "Launched in December 2012, VIP Room is the hottest new venue to hit Dubai�s sophisticated social scene. " +
				"This internationally renowned, flashy French nightclub brand epitomises the glamorous lifestyles led in St Tropez, " +
				"Paris and at Cannes Film Festival.  " +
				"With its celeb founder Jean-Roch, " +
				"the VIP Room has gained an worldwide reputation for drawing in an exclusively select clientele of high-end fashion socialites, " +
				"international jetsetters and stars. " +
				"Previous guests have included the likes of Kanye West, Johnny Depp, Rhianna and Madonna. " +
				"And its Dubai venue is no different � VIP Room has become the newest and most glamorous Business Bay hangout, " +
				"attracting an array of impeccably dressed guests looking to party in style." +
				"\n\n\n\nAddress: JW Marriott Marquis Dubai", 
				R.drawable.club27, new LatLng(25.18566, 55.257786));
		arrClub.add(club);
		
		club = new ClubInfo("Voda Bar", "Happy hours" +
				"\nEvery evening between 7.00pm and 9.00pm buy one drink and enjoy one with " +
				"our compliments as you unwind to deep chill and funky house. " +
				"\n\nLadies Night" +
				"\nEvery Tuesday enjoy a wide selection of complimentary beverages from 9.00pm to midnight with Dj Lance (Soul, Disco, Funk)" +
				"\n\nThursday Chill Out" +
				"\nEvery Thursday, it's  Chill Out and Deep House with DJ Da Sendri. Start  with the daily Happy Hours special offer, " +
				"buy one and get one free on selected house beverages from 7.00pm to 9.00pm. " +
				"\n\nAfter-Brunch" +
				"\nVoda Bar is the after-brunch destination.  " +
				"4.00pm to 9.00pm every Friday , and  you can enjoy a special 2 for 1 offer on selected house beverages." +
				"\n\nLocation : Ground level, left wing" +
				"\nCuisine : Cocktail barDress code : Smart Casual" +
				"(Gentlemen are requested to wear a shirt with collar, trousers and closed shoes. Shorts and trainers are not permitted)" +
				"\nTimes Open : daily 6pm-3am" +
				"\n\n\n\nAddress: Jumeirah Zabeel Saray, Palm Jumeirah, Dubai", 
				R.drawable.club28, new LatLng(25.097994, 55.123599));
		arrClub.add(club);
		
		club = new ClubInfo("Zinc", "Dress Code : Smart / Casual" +
				"\nTimes : Open daily 10pm-3am" +
				"\n\nWarning: Zinc is for those who like to party hard.  " +
				"As one of the original clubbing venues in Dubai known for its pacey nights out, " +
				"Zinc attracts the party animal crowd looking to dance all night long.  " +
				"Playing an array of music styles, the first-rate set of resident DJs know how to turn up those tunes and get the party started.  " +
				"It�s needless to say that the dance floor at Zinc Dubai is never empty." +
				"\n\n\nSpecial Offers" +
				"\n\n- Happy hour from 10pm until midnight." +
				"\n- 50% discount on all drinks for FACE members." +
				"\n-  50% discount on drinks all night for Emirates Engineer�s Club." +
				"\n- 50 % off drinks all night for Emirates Flight Catering employees.  " +
				"All other airline crew members receive 20% off drinks." +
				"\n* The above offers are valid every day except Wednesdays and Saturdays." +
				"\n- Zinc�s TWICE AS NICE promotion proposes a buy one get one free offer with every bottle purchased " +
				"before midnight for those with a table booking for 4 or more guests." +
				"\n\n\n\nAddress:Crowne Plaza Dubai, Sheikh Zayed road, Dubai", 
				R.drawable.club29, new LatLng(25.220183, 55.280437));
		arrClub.add(club);
		
		club = new ClubInfo("Zuma", "Located in the heart of Dubai in the international financial centre, " +
				"Zuma restaurant Dubai is the perfect venue for a speedy bite to eat, " +
				"business lunch or evening of fine dining with friends or colleagues.  " +
				"Priding itself on its contemporary Japanese cuisine, " +
				"Zuma offers a high class dining experience in the most sophisticated and stylish of settings.  " +
				"Its extensive menu offers a wide variety of choice, " +
				"including staples such as sushi and tempura, " +
				"as well as more unusual choices such as a selection of nigiri and black cod, " +
				"Korean chilli-marinated lamb chops and jumbo tiger prawn with yuzu pepper and spicy beef tenderloin. " +
				"Guests can also enjoy after-dinner cocktails in Zuma�s chilled-out lounge." +
				"\n\n\n\nAddress: Gate Village 06, Dubai",
				R.drawable.club30, new LatLng(25.2099, 55.275));
		arrClub.add(club);
	}
}
