package com.han.clubapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ClubAdapter extends ArrayAdapter<ClubInfo> {
	
    Activity parentActivity; 
    int layoutResourceId;    
    ArrayList<ClubInfo>  arrClubInfo = new ArrayList<ClubInfo>();
    
	public ClubAdapter(Context context, int layoutResourceId, ArrayList<ClubInfo> _arrClubInfo) {
        super(context, layoutResourceId, _arrClubInfo);
        this.layoutResourceId = layoutResourceId;
        this.parentActivity = (Activity) context;
        this.arrClubInfo = _arrClubInfo;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        LayoutInflater inflater = parentActivity.getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);
        
        TextView txtFirstName = (TextView) row.findViewById(R.id.first_name_textView);
        TextView txtLastName = (TextView) row.findViewById(R.id.last_name_textView);

        Typeface tfOswaldBold = Typeface.createFromAsset(parentActivity.getApplicationContext().getAssets(),"font/Oswald-Bold.ttf");
        Typeface tfOswaldLight = Typeface.createFromAsset(parentActivity.getApplicationContext().getAssets(),"font/Oswald-Light.ttf");
        Typeface tfOswaldReqular = Typeface.createFromAsset(parentActivity.getApplicationContext().getAssets(),"font/Oswald-Regular.ttf");
        Typeface tfUniAcc = Typeface.createFromAsset(parentActivity.getApplicationContext().getAssets(),"font/Uni_acc.ttf");

        txtFirstName.setTypeface(tfUniAcc);
        txtLastName.setTypeface(tfUniAcc);
        
        ClubInfo club = null;
        club = arrClubInfo.get(position);
        String name = club.title;
        
        if (parentActivity.getClass() == ClubListActivity.class) {
            String firstName = name.substring(0, 1);
            String lastName = name.substring(1);
            txtFirstName.setText(firstName.toUpperCase());
            txtLastName.setText(lastName.toUpperCase());
        }
        if (parentActivity.getClass() == ClubTopActivity.class) {
            String firstName = Integer.toString(position + 1) + " ";
            if (position < 9) firstName = "0" + firstName;
            String lastName = name;
            txtFirstName.setText(firstName.toUpperCase());
            txtLastName.setText(lastName.toUpperCase());
        }
        
        return row;
    }
}