package com.han.clubapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ClubTopActivity extends Activity {

	ListView lstClub;
	ArrayList<ClubInfo> arrClub = new ArrayList<ClubInfo>();

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_top);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {

		lstClub = (ListView) findViewById(R.id.top_club_listView);
	}
	
	private void initValue() {
		
		arrClub = new ArrayList<ClubInfo>();
		
		arrClub.add(MainActivity.arrClub.get(10));
		arrClub.add(MainActivity.arrClub.get(23));
		arrClub.add(MainActivity.arrClub.get(6));
		arrClub.add(MainActivity.arrClub.get(0));
		arrClub.add(MainActivity.arrClub.get(11));
		arrClub.add(MainActivity.arrClub.get(14));
		arrClub.add(MainActivity.arrClub.get(15));
		arrClub.add(MainActivity.arrClub.get(22));
		arrClub.add(MainActivity.arrClub.get(26));
		arrClub.add(MainActivity.arrClub.get(25));
		
		ClubAdapter adapter = new ClubAdapter(this, R.layout.club_row, arrClub);
		lstClub.setAdapter(adapter);
		
		lstClub.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	
            	ClubInfo club = arrClub.get(position);
        	    
//        	    if (position == 0) {
//                	club.bmp = BitmapFactory.decodeResource(ClubTopActivity.this.getResources(), R.drawable.club1);
//        	    } else if (position == 9) {
//                	club.bmp = BitmapFactory.decodeResource(ClubTopActivity.this.getResources(), R.drawable.club10);
//        	    } else {
//        	    	club.bmp = BitmapFactory.decodeResource(ClubTopActivity.this.getResources(), R.drawable.club1 + position + 1);
//        	    }
 
            	ClubDetailActivity.club = club;
            	ClubTopActivity.this.startActivity(new Intent(ClubTopActivity.this, ClubDetailActivity.class));
            	
            	club = null;
            }
        });
	}
	
	private void initEvent() {
		
	}
	
	public static int calculateInSampleSize(
	        BitmapFactory.Options options, int reqWidth, int reqHeight) {
	    // Raw height and width of image
	    final int height = options.outHeight;
	    final int width = options.outWidth;
	    int inSampleSize = 1;

	    if (height > reqHeight || width > reqWidth) {

	        // Calculate ratios of height and width to requested height and width
	        final int heightRatio = Math.round((float) height / (float) reqHeight);
	        final int widthRatio = Math.round((float) width / (float) reqWidth);

	        // Choose the smallest ratio as inSampleSize value, this will guarantee
	        // a final image with both dimensions larger than or equal to the
	        // requested height and width.
	        inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
	    }

	    return inSampleSize;
	}
}
