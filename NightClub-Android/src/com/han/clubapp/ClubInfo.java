package com.han.clubapp;

import com.google.android.gms.maps.model.LatLng;

import android.graphics.Bitmap;

public class ClubInfo {

	public String title;
	public String desc;
	public Bitmap bmp;
	public int res;
	public LatLng pos;
	
	public ClubInfo(String _title, String _desc, Bitmap _bmp) {
		title = _title;
		desc = _desc;
		bmp = _bmp;
	}
	
	public ClubInfo(String _title, String _desc, int _res, LatLng _pos) {
		title = _title;
		desc = _desc;
		res = _res;
		pos = _pos;
	}
}