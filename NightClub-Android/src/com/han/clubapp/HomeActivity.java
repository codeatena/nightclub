package com.han.clubapp;


import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HomeActivity extends Activity {
	
	GestureDetector tapGestureDetector;
	
	private ImagePagerAdapter mAdapter;
    public ViewPager mViewPager;
    
    public TextView txtTitle;
    public TextView txtDesc;
    
    public ArrayList<ClubInfo> arrClub = new ArrayList<ClubInfo>();
    public static int HOME_PAGE_COUNT = 7;
    
    Timer timer = null;
    private ImageView[] imgPageOrder;
    
    public final static Integer[] imageResIds = new Integer[] {
        R.drawable.home1, R.drawable.home2, R.drawable.home3,
        R.drawable.home4, R.drawable.home5, R.drawable.home6,
        R.drawable.home7};
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

//		startActivity(new Intent(this, SplashActivity.class));
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
        mViewPager = (ViewPager) findViewById(R.id.viewPager);
        
        txtTitle = (TextView) findViewById(R.id.first_name_textView);
        txtDesc = (TextView) findViewById(R.id.last_name_textView);
        
        imgPageOrder = new ImageView[HOME_PAGE_COUNT];
        
        imgPageOrder[0] = (ImageView) findViewById(R.id.page_order_imageView1);
        imgPageOrder[1] = (ImageView) findViewById(R.id.page_order_imageView2);
        imgPageOrder[2] = (ImageView) findViewById(R.id.page_order_imageView3);
        imgPageOrder[3] = (ImageView) findViewById(R.id.page_order_imageView4);
        imgPageOrder[4] = (ImageView) findViewById(R.id.page_order_imageView5);
        imgPageOrder[5] = (ImageView) findViewById(R.id.page_order_imageView6);
        imgPageOrder[6] = (ImageView) findViewById(R.id.page_order_imageView7);
        
        imgPageOrder[0].setVisibility(View.GONE);
        imgPageOrder[1].setVisibility(View.GONE);
        imgPageOrder[2].setVisibility(View.GONE);
        imgPageOrder[3].setVisibility(View.GONE);
        imgPageOrder[4].setVisibility(View.GONE);
        imgPageOrder[5].setVisibility(View.GONE);
        imgPageOrder[6].setVisibility(View.GONE);
        
        Bitmap bitmap1 = BitmapFactory.decodeResource(this.getResources(), R.drawable.home1);
        Bitmap bitmap2 = BitmapFactory.decodeResource(this.getResources(), R.drawable.home2);
        Bitmap bitmap3 = BitmapFactory.decodeResource(this.getResources(), R.drawable.home3);
        Bitmap bitmap4 = BitmapFactory.decodeResource(this.getResources(), R.drawable.home4);
        Bitmap bitmap5 = BitmapFactory.decodeResource(this.getResources(), R.drawable.home5);
        Bitmap bitmap6 = BitmapFactory.decodeResource(this.getResources(), R.drawable.home6);
        Bitmap bitmap7 = BitmapFactory.decodeResource(this.getResources(), R.drawable.home7);

        ArrayList<Bitmap> arrBitmap = new ArrayList<Bitmap>();
        
        arrBitmap.add(bitmap1);
        arrBitmap.add(bitmap2);
        arrBitmap.add(bitmap3);
        arrBitmap.add(bitmap4);
        arrBitmap.add(bitmap5);
        arrBitmap.add(bitmap6);
        arrBitmap.add(bitmap7);
        
        mAdapter = new ImagePagerAdapter(this, arrBitmap);
	}
	
	private void initEvent() {
		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {
	        public void onPageScrollStateChanged(int state) {}
	        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
	
	        public void onPageSelected(int position) {
	            // Check if this is the page you want.
	        	
	        	for (int i = 0; i < HOME_PAGE_COUNT; i++) {
	        		imgPageOrder[i].setImageResource(R.drawable.note_sound);
	        	}
	    		imgPageOrder[position].setImageResource(R.drawable.note_sound_sel);
	    		
	    		ClubInfo club = arrClub.get(position);
	    		txtTitle.setText(club.title);
	    		txtDesc.setText(club.desc);
	    		
//	        	Log.e("current page", Integer.toString(position));
	        }
		});
	    mViewPager.setAdapter(mAdapter);
		
		timer = new Timer();
		timer.schedule(new TimerTask() {
	        @Override
	        public void run() {

	        	mViewPager.getHandler().post(new Runnable() {
	    		    public void run() {
	    	        	int nNextPage = mViewPager.getCurrentItem() + 1;
	    	        	if (nNextPage == HOME_PAGE_COUNT) {
	    	        		nNextPage = 0;
	    	        	}
//	    		    	Log.d("HCW", getTime(count));
	    		    	mViewPager.setCurrentItem(nNextPage);
	    		    }
	    		});
	        }
		}, 5000, 5000);
		
		tapGestureDetector = new GestureDetector(this, new TapGestureListener());

		mViewPager.setOnTouchListener(new OnTouchListener() {
		        public boolean onTouch(View v, MotionEvent event) {
		            tapGestureDetector.onTouchEvent(event);
		            return false;
		        }
		});
		mViewPager.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				ClubInfo club = arrClub.get(mViewPager.getCurrentItem());
        	    
//        	    if (position == 0) {
//                	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club1);
//        	    } else if (position == 9) {
//                	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club10);
//        	    } else {
//        	    	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club1 + position + 1);
//        	    }
// 
            	ClubDetailActivity.club = club;
				HomeActivity.this.startActivity(new Intent(HomeActivity.this, ClubDetailActivity.class));
			}
        });
	}
	
	class TapGestureListener extends GestureDetector.SimpleOnGestureListener{
		
		 public boolean onSingleTapConfirmed(MotionEvent e) {
	           // Your Code here
			 ClubInfo club = arrClub.get(mViewPager.getCurrentItem());
     	    
//     	    if (position == 0) {
//             	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club1);
//     	    } else if (position == 9) {
//             	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club10);
//     	    } else {
//     	    	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club1 + position + 1);
//     	    }
//
			 ClubDetailActivity.club = club;
			 HomeActivity.this.startActivity(new Intent(HomeActivity.this, ClubDetailActivity.class));
			 return true;
		 }
	}
	
	private void initValue() {
    	for (int i = 0; i < 7; i++) {
    		imgPageOrder[i].setImageResource(R.drawable.note_sound);
    	}
    	
		imgPageOrder[0].setImageResource(R.drawable.note_sound_sel);
		arrClub = new ArrayList<ClubInfo>();
		
		arrClub.add(MainActivity.arrClub.get(0));
		arrClub.add(MainActivity.arrClub.get(10));
		arrClub.add(MainActivity.arrClub.get(11));
		arrClub.add(MainActivity.arrClub.get(3));
		arrClub.add(MainActivity.arrClub.get(23));
		arrClub.add(MainActivity.arrClub.get(4));
		arrClub.add(MainActivity.arrClub.get(14));
		
		txtTitle.setText(arrClub.get(0).title);
		txtDesc.setText(arrClub.get(0).desc);
	}
	
    public static class ImagePagerAdapter extends PagerAdapter {
    	
        private LayoutInflater inflater;
        private Activity _activity;
        private ArrayList<Bitmap> arrBitmap = new ArrayList<Bitmap>();

        public ImagePagerAdapter(Activity parent, ArrayList<Bitmap> _arrBitmap) {
            _activity = parent;
            arrBitmap = _arrBitmap;
        }

        @Override
        public int getCount() {
            return arrBitmap.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((RelativeLayout) object);
        }
        
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
        	ImageView imgDisplay;
            Button btnClose;
      
            inflater = (LayoutInflater) _activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container,
                    false);

            imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);

            Resources res = _activity.getResources();
            Bitmap bitmap = arrBitmap.get(position);
            BitmapDrawable bd = new BitmapDrawable(res, bitmap);
            
//            imgDisplay.setScaleType(ImageView.ScaleType.FIT_XY);
            imgDisplay.setImageDrawable(bd);
            
            ((ViewPager) container).addView(viewLayout);
      
            return viewLayout;
        }
        
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((RelativeLayout) object);
        }
    }
    
    public void onDestroy() { 
    	super.onDestroy();
//    	timer = null;
    	if (timer != null) {
    		timer.cancel();
    		timer = null;
    	}
    }
}
