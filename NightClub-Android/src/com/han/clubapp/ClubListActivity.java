package com.han.clubapp;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

public class ClubListActivity extends Activity {

	public EditText edtSearch;
	public ImageView imgSearch;
	public ListView lstClub;
	
	ArrayList<ClubInfo> arrClub = new ArrayList<ClubInfo>();
	ArrayList<ClubInfo> arrSearchClub = new ArrayList<ClubInfo>();
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		edtSearch = (EditText) findViewById(R.id.search_editText);
		imgSearch = (ImageView) findViewById(R.id.serach_imageView);
		lstClub = (ListView) findViewById(R.id.club_listView);
	}
	
	private void initValue() {
		
		arrClub = new ArrayList<ClubInfo>();
		arrClub = MainActivity.arrClub;
		arrSearchClub = new ArrayList<ClubInfo>();
		arrSearchClub = arrClub;
		
		ClubAdapter adapter = new ClubAdapter(this, R.layout.club_row, arrClub);
		lstClub.setAdapter(adapter);
		
		lstClub.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                
            	ClubInfo club = arrSearchClub.get(position);
        	    
//        	    if (position == 0) {
//                	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club1);
//        	    } else if (position == 9) {
//                	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club10);
//        	    } else {
//        	    	club.bmp = BitmapFactory.decodeResource(ClubListActivity.this.getResources(), R.drawable.club1 + position + 1);
//        	    }
// 
            	ClubDetailActivity.club = club;
            	ClubListActivity.this.startActivity(new Intent(ClubListActivity.this, ClubDetailActivity.class));
            }
        });
		
//		imgSearch.setOnClickListener(new Button.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				String strSearch = edtSearch.getText().toString();
//				
//				ArrayList<ClubInfo> arrSearchClub = new ArrayList<ClubInfo>();
//				for (int i = 0; i < arrClub.size(); i++) {
//					ClubInfo club = arrClub.get(i);
//					String title = club.title;
//					if (title.indexOf(strSearch) != -1) {
//						arrSearchClub.add(club);
//					}
//				}
//				ClubAdapter adapter = new ClubAdapter(ClubListActivity.this, R.layout.club_row, arrSearchClub);
//				lstClub.setAdapter(adapter);
//			}
//        });
		
		edtSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				String strSearch = edtSearch.getText().toString();
				
				arrSearchClub = new ArrayList<ClubInfo>();
				
				for (int i = 0; i < arrClub.size(); i++) {
					ClubInfo club = arrClub.get(i);
					String title = club.title.toUpperCase();
					if (title.indexOf(strSearch.toUpperCase()) != -1) {
						arrSearchClub.add(club);
					}
				}
				ClubAdapter adapter = new ClubAdapter(ClubListActivity.this, R.layout.club_row, arrSearchClub);
				lstClub.setAdapter(adapter);
			}
		});
	}
	
	private void initEvent() {

	}
}
