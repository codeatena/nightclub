package com.han.clubapp;

import java.util.ArrayList;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.app.Dialog;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class NearByActivity extends Activity implements 
	LocationListener
		{
	
	private GoogleMap mMap;
    
	ArrayList<ClubInfo> arrClub = new ArrayList<ClubInfo>();
//    private LongPressLocationSource mLocationSource;

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearby);
        setUpMapIfNeeded();
	}
	
//	 @Override
//	 protected void onResume() {
//        super.onResume();
//
//	 }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        if (mLocationClient != null) {
//            mLocationClient.disconnect();
//        }
//    }
	
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the MapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.nearby_map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }
    
    private void setUpMap() {
    	
		//mMap.setMyLocationEnabled(true);	
		//mMap.getUiSettings().setMyLocationButtonEnabled(true);

    	arrClub = new ArrayList<ClubInfo>();
		arrClub = MainActivity.arrClub;
    	for (int i = 0; i < arrClub.size(); i++) {
    		ClubInfo club = arrClub.get(i);
    		mMap.addMarker(new MarkerOptions().position(club.pos).title(club.title));
    	}
    	
    	int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
    	 
        // Showing status
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available
 
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
 
        }else {
        	mMap.setMyLocationEnabled(true);
        	 
            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
 
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();
 
            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);
 
            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);
 
            if(location!=null){
                onLocationChanged(location);
            }
            locationManager.requestLocationUpdates(provider, 20000, 0, this);

        }
//		if (mMap != null) {
//
//            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
//            	@Override
//	        	public void onMyLocationChange(Location arg0) {
//            		// 	TODO Auto-generated method stub
//                	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(arg0.getLatitude(), arg0.getLongitude()), 10));
//
//            		mMap.addMarker(new MarkerOptions().position(new LatLng(arg0.getLatitude(), arg0.getLongitude())).title("It's Me!"));
//
//            	}
//	        });
//        }
    	  // Move the camera instantly to location with a zoom of 15.
//    	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(25.164543, 55.204553), 10));

//    	mMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
    }

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		double latitude = arg0.getLatitude();
		 
        // Getting longitude of the current location
        double longitude = arg0.getLongitude();
 
        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);
 
        // Showing the current location in Google Map
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
 
        // Zoom in the Google Map
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
 
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
}
