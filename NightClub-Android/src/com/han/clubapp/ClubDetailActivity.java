package com.han.clubapp;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.MarkerOptions;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class ClubDetailActivity extends Activity {

	public static ClubInfo club = null;
	public static TextView txtTitle;
	public static TextView txtDesc;
	public static ImageView imgClub;
	
	private GoogleMap mMap;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_club_detail);
		
		initWidget();
		initValue();
		initEvent();
	}
	
	private void initWidget() {
		txtTitle = (TextView) findViewById(R.id.title_textView);
		txtDesc = (TextView) findViewById(R.id.desc_textView);
		imgClub = (ImageView) findViewById(R.id.club_imageView);
		
		setUpMapIfNeeded();
	}
	
	private void initValue() {
		
		Bitmap bmp = BitmapFactory.decodeResource(this.getResources(), club.res);

		txtTitle.setText(club.title);
		txtDesc.setText(club.desc);
		imgClub.setImageBitmap(bmp);
	}
	
	private void initEvent() {
		
	}
	
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the MapFragment.
            mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
    	mMap.addMarker(new MarkerOptions().position(club.pos).title(club.title));

    	  // Move the camera instantly to location with a zoom of 15.
    	mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(club.pos, 15));
    	
//    	mMap.animateCamera(CameraUpdateFactory.zoomTo(14), 2000, null);
    }
}
