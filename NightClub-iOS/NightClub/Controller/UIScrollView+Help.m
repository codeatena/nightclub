//
//  UIScrollView+Help.m
//  UIScrollViewCustomize
//
//  Created by wang chenglei on 4/21/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import "UIScrollView+Help.h"

@implementation UIScrollView (Help)

@dynamic myImagesArray;
static NSMutableArray *array = nil;

- (NSMutableArray *)myImagesArray {
    
    if (array == nil) {
        array = [[NSMutableArray alloc] init];
    }
    
    return array;
}

- (void)setImages:(NSMutableArray *)images {

    [[self myImagesArray] removeAllObjects];
    
    for (UIImage *image in images) {
        [[self myImagesArray] addObject:image];
    }
    
    CGSize size = self.frame.size;
    float new_width = size.width * self.myImagesArray.count;
    
    self.pagingEnabled = YES;
    [self setContentSize:CGSizeMake(new_width, size.height)];
    
    [self setShowsHorizontalScrollIndicator:NO];
    [self setShowsVerticalScrollIndicator:NO];
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    int k=0;
    for (UIImage *image in self.myImagesArray) {
//        [[UIImage imageNamed:@"button.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(k*size.width, 0, size.width, size.height)];
        imageView.image = image;
        [self addSubview:imageView];
        k++;
    }
    
    [self first];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize{
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)first {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    [self setContentOffset:CGPointZero];
    
    [UIView commitAnimations];
    [self setContentOffset:CGPointZero];
}

- (void)next {
    
    CGPoint offset = self.contentOffset;
    int pageNumber = offset.x / self.frame.size.width;
    
    pageNumber ++;
    if (pageNumber >= self.myImagesArray.count) {
        pageNumber = 0;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    [self setContentOffset:CGPointMake(pageNumber*self.frame.size.width, 0)];
    
    [UIView commitAnimations];
}

- (void)prev {
    
    CGPoint offset = self.contentOffset;
    int pageNumber = offset.x / self.frame.size.width;
    
    pageNumber --;
    if (pageNumber < 0) {
//        pageNumber = self.myImagesArray.count-1;
        pageNumber = 0;
    }
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    [self setContentOffset:CGPointMake(pageNumber*self.frame.size.width, 0)];
    
    [UIView commitAnimations];
}

- (void)last {
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    [self setContentOffset:CGPointMake((self.myImagesArray.count-1)*self.frame.size.width, 0)];
    
    [UIView commitAnimations];
}

@end
