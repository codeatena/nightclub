//
//  UIScrollView+Help.h
//  UIScrollViewCustomize
//
//  Created by wang chenglei on 4/21/14.
//  Copyright (c) 2014 John. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Help)

@property (nonatomic, strong) NSMutableArray *myImagesArray;

- (void)setImages:(NSMutableArray *)images;

- (void)first;
- (void)next;
- (void)prev;
- (void)last;

@end
