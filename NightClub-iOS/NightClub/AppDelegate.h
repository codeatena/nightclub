//
//  AppDelegate.h
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, retain) NSMutableArray* arrAllClubs;
@property (nonatomic, retain) NSMutableArray* arrTopClubs;
@property (nonatomic, retain) NSMutableArray* arrHomeClubs;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
