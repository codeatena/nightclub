//
//  ClubsViewController.h
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClubsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView* myTableView;
    
    NSMutableArray* arrClubs; /// test commit
}

- (IBAction)onSearch:(id)sender;
- (IBAction)onChange:(id)sender;

@end
