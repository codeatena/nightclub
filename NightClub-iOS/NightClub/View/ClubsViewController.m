//
//  ClubsViewController.m
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ClubsViewController.h"
#import "AppDelegate.h"
#import "ClubTableViewCell.h"
#import "ClubDetailViewController.h"

@interface ClubsViewController ()

@end

@implementation ClubsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    arrClubs = [[NSMutableArray alloc] init];
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    arrClubs = delegate.arrAllClubs;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSLog(@"clubs count = %d", arrClubs.count);
    return [arrClubs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"clubCell";

    ClubTableViewCell *cell = (ClubTableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    ClubInf* club = [arrClubs objectAtIndex:indexPath.row];
    
    [cell setClubInf:club INDEXL:-1];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClubInf* club = [arrClubs objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"modal_clubs_detail" sender:club];

    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString:@"modal_clubs_detail"]) {
        
        ClubDetailViewController *detail = (ClubDetailViewController *)[segue destinationViewController];
        
        ClubInf* club = (ClubInf*) sender;
        [detail setClubInf:club];
    }
}

- (IBAction)onSearch:(id)sender
{
    UITextField* txtField = (UITextField*) sender;
    [txtField resignFirstResponder];
}

- (IBAction)onChange:(id)sender;
{
    UITextField* txtField = (UITextField*) sender;
    NSLog(@"%@", txtField.text);
    
    arrClubs = [[NSMutableArray alloc] init];
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    NSString* key = [txtField.text lowercaseString];
    
    for (ClubInf* club in delegate.arrAllClubs) {
        NSString* str = [club.title lowercaseString];
        if ([str rangeOfString:key].location != NSNotFound || [key isEqualToString:@""]) {
            [arrClubs addObject:club];
        }
    }
    
    [myTableView reloadData];
}

@end
