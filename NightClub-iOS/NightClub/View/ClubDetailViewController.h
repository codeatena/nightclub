//
//  ClubDetailViewController.h
//  NightClub
//
//  Created by JinSung Han on 4/22/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ClubInf.h"

@interface ClubDetailViewController : UIViewController
{
    IBOutlet UIImageView* imgClub;
    IBOutlet UILabel*   lblTitle;
    IBOutlet UILabel*   lblDesc;
    IBOutlet MKMapView* mapView;
    IBOutlet UIScrollView* scrollPage;
    
    ClubInf* myClub;
}

- (void) setClubInf:(ClubInf*) clubInf;
- (IBAction)onCancel:(id)sender;

@end
