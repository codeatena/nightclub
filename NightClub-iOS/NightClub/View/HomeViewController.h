//
//  HomeViewController.h
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+Help.h"

@interface HomeViewController : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UILabel* lblTitle;
    IBOutlet UILabel* lblDesc;
    
    IBOutlet UIScrollView* scrollImage;
    IBOutlet UIScrollView* scrollPage;
    
    int curPageNumber; // page number
    NSMutableArray* arrClub;
    NSTimer* timer;
}

@end
