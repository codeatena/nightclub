//
//  NearByViewController.m
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "NearByViewController.h"
#import "ClubInf.h"
#import "AppDelegate.h"

@interface NearByViewController ()

@end

@implementation NearByViewController

@synthesize mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    NSMutableArray* arrClub = [[NSMutableArray alloc] init];
    arrClub = delegate.arrAllClubs;
    
    for (ClubInf* club in arrClub) {
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        point.coordinate = club.pos;
        point.title = club.title;
//        point.subtitle = @"I'm here!!!";

        [self.mapView addAnnotation:point];
    }
    
//    ClubInf* club = [arrClub objectAtIndex:0];
    //[self.mapView setCenterCoordinate:club.pos animated:YES];
//    [self.mapView setUserInteractionEnabled:YES];
//    [self.mapView showsUserLocation];
    
    [self.mapView setShowsUserLocation:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 500, 500);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
}

@end
