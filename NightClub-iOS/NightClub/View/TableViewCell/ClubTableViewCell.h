//
//  ClubTableViewCell.h
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClubInf.h"

@interface ClubTableViewCell : UITableViewCell
{
    IBOutlet    UILabel*    lblSecond;
    
    ClubInf* myClubInf;///
}

- (void) setClubInf:(ClubInf*) clubInf INDEXL:(int) index;

@end
