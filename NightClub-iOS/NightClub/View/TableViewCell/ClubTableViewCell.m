//
//  ClubTableViewCell.m
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ClubTableViewCell.h"
#import "ClubsViewController.h"

#import "define.h"

@implementation ClubTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setClubInf:(ClubInf*) clubInf INDEXL:(int) index
{
    [self.contentView setBackgroundColor:UIColorFromRGB(0x242424)];

    UIFont *font = [UIFont fontWithName:@"Universal Accreditation" size:35];

    [lblSecond setFont:font];

    if (index == -1) {
        NSString* str = [clubInf.title uppercaseString];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]  initWithString:str];
        [attributedString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xe32a30) range:NSMakeRange(0, 1)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(1, str.length - 1)];
        lblSecond.attributedText = attributedString;
    } else {
        NSString* strIndex = [NSString stringWithFormat:@"%02d", index + 1];
        NSString* str = [NSString stringWithFormat:@"%@ %@", strIndex, [clubInf.title uppercaseString]];
        
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]  initWithString:str];
        [attributedString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0xe32a30) range:NSMakeRange(0, 2)];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(2, str.length - 2)];
        lblSecond.attributedText = attributedString;
    }
}

@end
