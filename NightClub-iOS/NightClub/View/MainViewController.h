//
//  MainViewController.h
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController
{
    IBOutlet    UIImageView*    imgHome;
    IBOutlet    UIImageView*    imgClubs;
    IBOutlet    UIImageView*    imgTop;
    IBOutlet    UIImageView*    imgNearBy;
    
    IBOutlet    UILabel*        lblHome;
    IBOutlet    UILabel*        lblClubs;
    IBOutlet    UILabel*        lblTop;
    IBOutlet    UILabel*        lblNearBy;
    
    IBOutlet    UIButton*       btnHome;
    IBOutlet    UIButton*       btnClubs;
    IBOutlet    UIButton*       btnTop;
    IBOutlet    UIButton*       btnNearBy;
    
    IBOutlet    UIView*         viewHome;
    IBOutlet    UIView*         viewClubs;
    IBOutlet    UIView*         viewTop;
    IBOutlet    UIView*         viewNearBy;
}

- (IBAction)onHome:(id)sender;
- (IBAction)onClubs:(id)sender;
- (IBAction)onTop:(id)sender;
- (IBAction)onNearBy:(id)sender;

@end
