//
//  NearByViewController.h
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface NearByViewController : UIViewController<MKMapViewDelegate>
{
    IBOutlet MKMapView* mapView;
}

@property (nonatomic, strong) IBOutlet MKMapView *mapView;

@end
