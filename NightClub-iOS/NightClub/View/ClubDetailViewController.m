//
//  ClubDetailViewController.m
//  NightClub
//
//  Created by JinSung Han on 4/22/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ClubDetailViewController.h"
#import "define.h"

@interface ClubDetailViewController ()

@end

@implementation ClubDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [scrollPage setBackgroundColor:UIColorFromRGB(0x333333)];
    [lblTitle setTextColor:UIColorFromRGB(0xa4a4a4)];
    [lblDesc setTextColor:UIColorFromRGB(0xa4a4a4)];

    lblTitle.text = myClub.title;
    lblDesc.text = myClub.desc;
    imgClub.image = myClub.image;
    
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = myClub.pos;
    point.title = myClub.title;
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(myClub.pos, 3000, 3000);
    MKCoordinateRegion adjustedRegion = [mapView regionThatFits:viewRegion];
    [mapView addAnnotation:point];
    [mapView setRegion:adjustedRegion animated:YES];

    [lblDesc sizeToFit];
    
    CGRect rtDesc = lblDesc.frame;
    CGRect rtMap = mapView.frame;
    
    float y = rtDesc.origin.y + rtDesc.size.height + 10;
    mapView.frame = CGRectMake(rtMap.origin.x, y, rtMap.size.width, rtMap.size.height);
    
    float newHeight = y + rtMap.size.height + 10;
    
    [scrollPage setContentSize:CGSizeMake(320, newHeight)];
    [mapView setCenterCoordinate:myClub.pos animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setClubInf:(ClubInf*) clubInf
{
    myClub = clubInf;
}

- (IBAction)onCancel:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
