//
//  MainViewController.m
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "MainViewController.h"
#import "define.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self refeshTab];
    [self onHome:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onHome:(id)sender
{
    [self refeshTab];
    imgHome.image = [UIImage imageNamed:@"tab_home_selected.png"];
    [lblHome setTextColor:UIColorFromRGB(0xeebe2e)];
    viewHome.hidden = NO;
}

- (IBAction)onClubs:(id)sender
{
    [self refeshTab];
    imgClubs.image = [UIImage imageNamed:@"tab_club_selected.png"];
    [lblClubs setTextColor:UIColorFromRGB(0xeebe2e)];
    viewClubs.hidden = NO;
}

- (IBAction)onTop:(id)sender
{
    [self refeshTab];
    imgTop.image = [UIImage imageNamed:@"tab_top_selected.png"];
    [lblTop setTextColor:UIColorFromRGB(0xeebe2e)];
    viewTop.hidden = NO;
}

- (IBAction)onNearBy:(id)sender
{
    [self refeshTab];
    imgNearBy.image = [UIImage imageNamed:@"tab_nearby_selected.png"];
    [lblNearBy setTextColor:UIColorFromRGB(0xeebe2e)];
    viewNearBy.hidden = NO;
}

- (void) refeshTab
{
    imgHome.image = [UIImage imageNamed:@"tab_home_general.png"];
    [lblHome setTextColor:UIColorFromRGB(0xffffff)];

    imgClubs.image = [UIImage imageNamed:@"tab_club_general.png"];
    [lblClubs setTextColor:UIColorFromRGB(0xffffff)];

    imgTop.image = [UIImage imageNamed:@"tab_top_general.png"];
    [lblTop setTextColor:UIColorFromRGB(0xffffff)];

    imgNearBy.image = [UIImage imageNamed:@"tab_nearby_general.png"];
    [lblNearBy setTextColor:UIColorFromRGB(0xffffff)];
    
    
    viewHome.hidden = YES;
    viewClubs.hidden = YES;
    viewTop.hidden = YES;
    viewNearBy.hidden = YES;
}

@end
