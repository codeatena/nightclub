//
//  TopViewController.h
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView* myTableView;
    NSMutableArray* arrClubs;
}

@end
