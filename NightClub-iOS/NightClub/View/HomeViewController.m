//
//  HomeViewController.m
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "HomeViewController.h"
#import "AppDelegate.h"
#import "ClubInf.h"
#import "define.h"
#import "ClubDetailViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    curPageNumber = 0;
    arrClub = [[NSMutableArray alloc] init];
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    arrClub = delegate.arrHomeClubs;
    
    [self setSlideImages];
    
    [lblTitle setTextColor:UIColorFromRGB(0xa4a4a4)];
    [lblDesc setTextColor:UIColorFromRGB(0xa4a4a4)];
    [scrollPage setBackgroundColor:UIColorFromRGB(0x333333)];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(openClub:)];
    singleTap.numberOfTapsRequired = 1;
    [scrollImage addGestureRecognizer:singleTap];
    
    ClubInf* club = [arrClub objectAtIndex:0];
    lblTitle.text = club.title;
    lblDesc.text = club.desc;
    
    [self resetScrollPage];
    [NSTimer scheduledTimerWithTimeInterval: 5.0f target: self selector:@selector(nextSlide) userInfo:nil repeats: YES];
}

- (void) resetScrollPage
{
    [lblDesc sizeToFit];

    float height = lblDesc.frame.origin.y + lblDesc.frame.size.height + 10;
    
    [scrollPage setContentSize:CGSizeMake(scrollPage.frame.size.width, height)];
}

- (void) openClub:(id) sender
{
    NSLog(@"openClub");
    ClubInf* club = [arrClub objectAtIndex:curPageNumber];

    [self performSegueWithIdentifier:@"modal_home_detail" sender:club];
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString:@"modal_home_detail"]) {
        
        ClubDetailViewController *detail = (ClubDetailViewController *)[segue destinationViewController];

        ClubInf* club = (ClubInf*) sender;
        [detail setClubInf:club];
    }
}

- (void) setSlideImages
{
    UIImage* image1 = [UIImage imageNamed:@"home1.jpg"];
    UIImage* image2 = [UIImage imageNamed:@"home2.jpg"];
    UIImage* image3 = [UIImage imageNamed:@"home3.jpg"];
    UIImage* image4 = [UIImage imageNamed:@"home4.jpg"];
    UIImage* image5 = [UIImage imageNamed:@"home5.jpg"];
    UIImage* image6 = [UIImage imageNamed:@"home6.jpg"];
    UIImage* image7 = [UIImage imageNamed:@"home7.jpg"];

    NSMutableArray* arrImages = [[NSMutableArray alloc] init];
    
    [arrImages addObject:image1];
    [arrImages addObject:image2];
    [arrImages addObject:image3];
    [arrImages addObject:image4];
    [arrImages addObject:image5];
    [arrImages addObject:image6];
    [arrImages addObject:image7];
    
    [scrollImage setImages:arrImages];
}

- (void) scrollViewDidScroll:(UIScrollView *) scrollView {
    
    if (scrollView == scrollImage) {
        CGPoint offset = scrollImage.contentOffset;
        int pageNumber = offset.x / scrollImage.frame.size.width;
        if (curPageNumber != pageNumber) {
            NSLog(@"page changed %d", pageNumber);
            curPageNumber = pageNumber;
            ClubInf* club = [arrClub objectAtIndex:pageNumber];
            
            lblTitle.text = club.title;
            lblDesc.text = club.desc;
            
            [self resetScrollPage];
        }
    }
}

- (void) nextSlide
{
    [scrollImage next];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
