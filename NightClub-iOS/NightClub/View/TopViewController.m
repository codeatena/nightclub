//
//  TopViewController.m
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "TopViewController.h"
#import "AppDelegate.h"
#import "ClubTableViewCell.h"
#import "ClubDetailViewController.h"

@interface TopViewController ()

@end

@implementation TopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    arrClubs = [[NSMutableArray alloc] init];
    arrClubs = [[NSMutableArray alloc] init];
    AppDelegate* delegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
    
    arrClubs = delegate.arrTopClubs;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger) tableView : (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSLog(@"clubs count = %d", arrClubs.count);
    return [arrClubs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"clubCell";
    
    ClubTableViewCell *cell = (ClubTableViewCell*) [tableView dequeueReusableCellWithIdentifier: cellIdentifier forIndexPath:indexPath];
    ClubInf* club = [arrClubs objectAtIndex:indexPath.row];

    [cell setClubInf:club INDEXL:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClubInf* club = [arrClubs objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"modal_top_detail" sender:club];
    
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    if ([segue.identifier isEqualToString:@"modal_top_detail"]) {
        
        ClubDetailViewController *detail = (ClubDetailViewController *)[segue destinationViewController];
        
        ClubInf* club = (ClubInf*) sender;
        [detail setClubInf:club];
    }
}

@end
