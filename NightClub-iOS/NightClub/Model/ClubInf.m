//
//  ClubInf.m
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import "ClubInf.h"

@implementation ClubInf

@synthesize title, desc, image, fileName, pos;

- (id) initWithTitle:(NSString*) _title DESCRIPTION:(NSString*) _desc FILENAME:(NSString*) _fileName LATITUDE:(double) lat LONGITUDE:(double) lon
{
    self = [super init];
    
    if (self != nil) {
        title = _title;
        desc = _desc;
        fileName = _fileName;
        image = [UIImage imageNamed:_fileName];
        
        pos.latitude = lat;
        pos.longitude = lon;
    }
    
    return self;
}

@end
