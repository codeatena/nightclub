//
//  ClubInf.h
//  NightClub
//
//  Created by JinSung Han on 4/21/14.
//  Copyright (c) 2014 choe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ClubInf : NSObject

@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* desc;
@property (nonatomic, retain) UIImage* image;
@property (nonatomic, retain) NSString* fileName;
@property (nonatomic) CLLocationCoordinate2D pos;

- (id) initWithTitle:(NSString*) _title DESCRIPTION:(NSString*) _desc FILENAME:(NSString*) _fileName LATITUDE:(double) lat LONGITUDE:(double) lon;

@end
